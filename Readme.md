# News App

***You can Install latest News app from below***

[![News App](https://img.shields.io/badge/NewsApp-APK-blue.svg?style=for-the-badge&logo=android&color=blue)](https://drive.google.com/file/d/1o6b-kTofBzrzZ74yqdXQTqQMA_u54AcE/view?usp=sharing)
 

## About
It simply loads TOP HEADLINES and show it with the help of epoxy.
This app demonstrates the perfect use of Epoxy Library.

- Clean and Simple Material UI.

*API is used in this app [News API](https://newsapi.org)*

## Built With 🛠
- [Kotlin](https://kotlinlang.org/) - First class and official programming language for Android development.
- [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) - For asynchronous and more..
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture) - Collection of libraries that help you design robust, testable, and maintainable apps.

  - [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Stores UI-related data that isn't destroyed on UI changes. 

  - [ViewBinding](https://developer.android.com/topic/libraries/view-binding) - Generates a binding class for each XML layout file present in that module and allows you to more easily write code that interacts with views.

- [Dependency Injection](https://developer.android.com/training/dependency-injection)  
  - [Hilt-Dagger](https://dagger.dev/hilt/) - Standard library to incorporate Dagger dependency injection into an Android application.

  - [Hilt-ViewModel](https://developer.android.com/training/dependency-injection/hilt-jetpack) - DI for injecting `ViewModel`.

- [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java.

- [Jetpack Navigation](https://developer.android.com/guide/navigation) - Navigation refers to the interactions that allow users to navigate across, into, and back out from the different pieces of content within your app.

- [Material Components for Android](https://github.com/material-components/material-components-android) - Modular and customizable Material Design UI components for Android.

