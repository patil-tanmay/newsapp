package com.tanmay.newsapp

import androidx.lifecycle.ViewModel
import com.tanmay.newsapp.api.NewsApi
import com.tanmay.newsapp.data.NewsResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.receiveAsFlow
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val api : NewsApi
): ViewModel() {

}