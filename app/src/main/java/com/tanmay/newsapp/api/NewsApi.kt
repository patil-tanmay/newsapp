package com.tanmay.newsapp.api

import com.tanmay.newsapp.data.NewsResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface NewsApi {

    @GET("top-headlines")
    suspend fun getNewArticles(
        @Query("country") country: String = "us",
        @Query("category") category : String,
        ) : NewsResponse
}