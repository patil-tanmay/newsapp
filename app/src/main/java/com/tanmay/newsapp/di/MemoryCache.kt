package com.tanmay.newsapp.di

import com.tanmay.newsapp.data.Article
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import javax.inject.Inject

@Module
@InstallIn(ActivityRetainedComponent::class)
class MemoryCache @Inject constructor() {
    var technologyNews: List<Article> = emptyList<Article>()
    var entertainmentNews: List<Article> = emptyList()
    var businessNews: List<Article> = emptyList()
}