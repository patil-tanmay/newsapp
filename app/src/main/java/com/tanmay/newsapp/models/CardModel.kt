package com.tanmay.newsapp.models

import android.view.View
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.Glide
import com.tanmay.newsapp.R
import com.tanmay.newsapp.databinding.ItemNewsBinding

@EpoxyModelClass(layout = R.layout.item_news)
abstract class CardModel : EpoxyModelWithHolder<CardModel.CardHolder>(){

    @field:EpoxyAttribute
    open var title1: CharSequence? = null

    @field:EpoxyAttribute
    open var publishedDate1: CharSequence?=null

    @field:EpoxyAttribute
    open var imageUrl: String? = null

    @field:EpoxyAttribute
    open var description: String?=null

    @EpoxyAttribute
    open lateinit var rootOnClickListener: (description: String) -> Unit

    override fun bind(holder: CardHolder) {
        holder.binding.apply {
            if(imageUrl!=null) {
                Glide.with(imgView)
                    .load(imageUrl)
                    .into(imgView)
            }

            title.text = title1
            publishedDate.text = publishedDate1
        }

        holder.binding.root.setOnClickListener{
            rootOnClickListener(description!!)
        }

    }

    class CardHolder : EpoxyHolder() {

        lateinit var binding: ItemNewsBinding
            private set

        override fun bindView(itemView: View) {
            binding = ItemNewsBinding.bind(itemView)
        }
    }

}