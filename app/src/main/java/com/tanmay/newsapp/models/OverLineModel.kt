package com.tanmay.newsapp.models

import android.view.View
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.Glide
import com.tanmay.newsapp.R
import com.tanmay.newsapp.databinding.ItemNewsBinding
import com.tanmay.newsapp.databinding.OverlineBinding

@EpoxyModelClass(layout = R.layout.overline)
abstract class OverLineModel : EpoxyModelWithHolder<OverLineModel.LineHolder>(){

    @field:EpoxyAttribute
    open var value: CharSequence?=null

    override fun bind(holder: LineHolder) {
        holder.binding.root.text = value
    }

    class LineHolder : EpoxyHolder() {

        lateinit var binding: OverlineBinding
            private set

        override fun bindView(itemView: View) {
            binding = OverlineBinding.bind(itemView)
        }
    }

}