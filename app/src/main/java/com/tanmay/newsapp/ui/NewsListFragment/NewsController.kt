package com.tanmay.newsapp.ui.NewsListFragment

import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.carousel
import com.tanmay.newsapp.data.Article
import com.tanmay.newsapp.models.CardModel_
import com.tanmay.newsapp.models.overLine
import java.util.concurrent.CopyOnWriteArrayList

class NewsController(
    private val onRootClick: (article: Article) -> Unit
) : AsyncEpoxyController() {

    private var businessNews: CopyOnWriteArrayList<CardModel_> = CopyOnWriteArrayList()
    private var technologyNews: CopyOnWriteArrayList<CardModel_> = CopyOnWriteArrayList()
    private var entertainmentNews: CopyOnWriteArrayList<CardModel_> = CopyOnWriteArrayList()

    fun setBusinessNewsData(articles: List<Article>) {
        businessNews.clear()
        val news = articles.map {
            CardModel_().id("LatestArticles${it.publishedAt}")
                .imageUrl(it.urlToImage)
                .title1(it.title)
                .publishedDate1(it.publishedAt)
                .description(it.description)
                .rootOnClickListener { desc ->
                    val position = articles.map { article-> article.description }.indexOf(desc)
                    onRootClick(articles[position])
                }
        }
        businessNews.addAll(news)
        requestModelBuild()
    }

    fun setTechnologyNews(articles: List<Article>){
        technologyNews.clear()
        val news = articles.map {
            CardModel_().id("LatestArticles${it.publishedAt}")
                .imageUrl(it.urlToImage)
                .title1(it.title)
                .publishedDate1(it.publishedAt)
                .description(it.description)
                .rootOnClickListener { desc ->
                    val position = articles.map { article-> article.description }.indexOf(desc)
                    onRootClick(articles[position])
                }
        }
        technologyNews.addAll(news)
        requestModelBuild()
    }

    fun setEntertainmentNews(articles: List<Article>){
        entertainmentNews.clear()
        val news = articles.map {
            CardModel_().id("LatestArticles${it.publishedAt}")
                .imageUrl(it.urlToImage)
                .title1(it.title)
                .publishedDate1(it.publishedAt)
                .description(it.description)
                .rootOnClickListener { desc ->
                    val position = articles.map { article-> article.description }.indexOf(desc)
                    onRootClick(articles[position])
                }
        }
        entertainmentNews.addAllAbsent(news)
        requestModelBuild()
    }


    override fun buildModels() {
        if (businessNews.isNotEmpty()) {
            val newsArticles = businessNews
            overLine {
                id("NewsGenre")
                value("Top HeadLines")
            }

            overLine {
                id("BusinessGenre")
                value("Business")
            }

            carousel {
                id("carousel_id_business")
                models(newsArticles)
                numViewsToShowOnScreen(2f)
            }
        }

        if(technologyNews.isNotEmpty()) {

            overLine {
                id("TechnologyGenre")
                value("Technology")
            }

            carousel {
                id("carousel_id_technology")
                models(this@NewsController.technologyNews)
                numViewsToShowOnScreen(2f)
            }
        }

        if (entertainmentNews.isNotEmpty()) {

            overLine {
                id("entertainment_genre")
                value("Entertainment")
            }

            carousel {
                id("carousel_id_entertainment")
                models(this@NewsController.entertainmentNews)
                numViewsToShowOnScreen(2f)
            }
        }

    }


}