package com.tanmay.newsapp.ui.NewsListFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.tanmay.newsapp.R
import com.tanmay.newsapp.data.Article
import com.tanmay.newsapp.databinding.FragmentNewsListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NewsListFragment : Fragment(R.layout.fragment_news_list) {

    private var _binding: FragmentNewsListBinding? = null
    private val binding get() = _binding!!

    private val viewModel: NewsListViewmodel by viewModels()

    private lateinit var controller: NewsController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentNewsListBinding.bind(view)

        controller = NewsController(::onRootClick)

        binding.recView.setController(controller)
        binding.recView.setHasFixedSize(true)

        viewLifecycleOwner.lifecycleScope.launch {

            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.getBusinessNews().collect {
                        controller.setBusinessNewsData(it)
                    }
                }

                launch {
                    viewModel.getEntertainmentNews().collect{
                        controller.setEntertainmentNews(it)
                    }
                }

                launch {
                    viewModel.getTechnologyNews().collect{
                        controller.setTechnologyNews(it)
                    }
                }

                launch {
                    viewModel.error.collect {
                        Log.d("TAG", "onViewCreated: $it ")
                        Toast.makeText(context, "Error: $it", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun onRootClick(article: Article){
        findNavController().navigate(NewsListFragmentDirections.actionNewsListFragmentToDetailNewsFragment(article))
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}