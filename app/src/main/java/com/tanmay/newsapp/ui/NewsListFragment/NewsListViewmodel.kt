package com.tanmay.newsapp.ui.NewsListFragment

import androidx.lifecycle.ViewModel
import com.tanmay.newsapp.api.NewsApi
import com.tanmay.newsapp.di.MemoryCache
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.receiveAsFlow
import javax.inject.Inject

@HiltViewModel
class NewsListViewmodel @Inject constructor(
    private val api: NewsApi,
    private val memoryCache: MemoryCache
) : ViewModel() {

    private val _error = Channel<Exception>()
    val error get() = _error.receiveAsFlow()

    fun getBusinessNews() = flow {
        try {
            if (memoryCache.businessNews.isEmpty()) {
                val d = api.getNewArticles(category = "business")
                memoryCache.businessNews = d.articles
                emit(d.articles)
            } else {
                emit(memoryCache.businessNews)
            }
        } catch (e: Exception) {
            _error.send(e)
        }
    }

    fun getTechnologyNews() = flow {
        try {
            if (memoryCache.technologyNews.isEmpty()) {
                val d = api.getNewArticles(category = "technology")
                memoryCache.technologyNews = d.articles
                emit(d.articles)
            } else {
                emit(memoryCache.technologyNews)
            }
        } catch (e: Exception) {
            _error.send(e)
        }
    }

    fun getEntertainmentNews() = flow {
        try {
            if (memoryCache.entertainmentNews.isEmpty()) {
                val d = api.getNewArticles(category = "entertainment")
                memoryCache.entertainmentNews = d.articles
                emit(d.articles)
            } else {
                emit(memoryCache.entertainmentNews)
            }
        } catch (e: Exception) {
            _error.send(e)
        }
    }
}