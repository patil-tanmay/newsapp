package com.tanmay.newsapp.ui.detailNewsFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.tanmay.newsapp.R
import com.tanmay.newsapp.databinding.FragmentDetailNewsBinding


class DetailNewsFragment : Fragment(R.layout.fragment_detail_news) {

    private var _binding: FragmentDetailNewsBinding? = null
    private val binding get() = _binding!!

    private val args: DetailNewsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentDetailNewsBinding.bind(view)

        val article = args.articleData

        Glide.with(this)
            .load(article.urlToImage)
            .into(binding.imgView)

        binding.title.text = article.title
        binding.publishedDate.text = article.publishedAt
        binding.desc.text = article.description

        binding.backRow.setOnClickListener {
            activity?.onBackPressed()
        }

    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }


}