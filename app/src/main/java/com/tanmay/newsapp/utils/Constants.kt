package com.tanmay.newsapp.utils

import com.tanmay.newsapp.BuildConfig


object Constants {

    val BASE_URL = "https://newsapi.org/v2/"

    const val API_KEY = BuildConfig.NEWS_API

}